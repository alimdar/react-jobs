This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Introduction

React-jobs project for Talentspace front-end developer application. 

Application allows users to load list of jobs and filter the results by applying filters to job query.  
To narrow results, select filter from dropdown menu or search a term in title of the job item. Search submits on enter. 

Options for filters are fetched from available tables: `cities (from jobs table)`, `companies` and `investors`. 

Filters can be reset one by one or all at the time - Reset all. 

Pages with more that 10 results have `Fetch more` option that is available untill all results have been fetched. 

> Note on React Select: Component used for dropdown is React-Select.
> At the moment the library is showing warnings for deprecated methods. 
> The warnings will have no impact on component performance.
> The issues can be followed [here](https://github.com/JedWatson/react-select/issues/3915) and [still open here](https://github.com/JedWatson/react-select/issues/4094).


## How to run the project

There are two ways how to run the project:

1.) Using only the docker-compose file: 
- `docker-compose up --build`
- This will start the React app on port 8000 and Hasura on port 8080


2.) Running a local version of the React app alongside the docker container 
- `docker-compose up -d --build`
- `yarn install`
- `yarn start`
- This will start the local React app on port 3000 and Hasura on port 8080

####To run tests run:

- `yarn test`


## Description of commands

### `docker-compose up -d --build`

Builds and starts the containers for the React app, Postgres database and Hasura Console alongside each other.
It can take a few seconds after the containers have started until the Database is fully initialized and seeded.
* Open [http://localhost:8000](http://localhost:8000) for the **React app**
* Open [http://localhost:8080](http://localhost:8080) for the **Hasura Console**

### `yarn start`

Runs the app in the local development mode. If you prefer this to the dockerized React app. <br />
Open [http://localhost:3000](http://localhost:3000) to view the **React app**

You **still have to run the docker containers** at the same time, to make the Database + Hasura Console available.

## Executing commands in the React container (e.g. for adding packages)
If you want to add a new package, you can do this in the project. But it won't automatically be available in the React container without rebuilding.

To make an added package available without rebuilding you can run yarn install directly in the container:

`docker exec -it coding-challenge yarn install`

## About Hasura GraphQL Engine

Hasura GraphQL Engine is a blazing-fast GraphQL server that gives you **instant, realtime GraphQL APIs over Postgres**, with [**webhook triggers**](event-triggers.md) on database events, and [**remote schemas**](remote-schemas.md) for business logic.

Hasura helps you build GraphQL apps backed by Postgres or incrementally move to GraphQL for existing applications using Postgres.

Read more at [hasura.io](https://hasura.io) and the [docs](https://hasura.io/docs).



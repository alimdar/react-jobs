import React, {useMemo, useReducer} from "react";
import { ApolloProvider } from "@apollo/react-hooks";
import ApolloClient from "./apollo";
import JobList from "./components/job-list-components/job-list/JobList";
import Filters from "./components/filter-components/filters/Filters";
import {FilterContext} from "./store/context";
import {initialState, reducer} from "./store/reducer";
import styled from "styled-components";

function App() {

  const [state, dispatch] = useReducer(reducer, initialState);
  const contextValue = useMemo(() => ({ state, dispatch }),[state, dispatch]);

  return (
    <ApolloProvider client={ApolloClient}>
      <FilterContext.Provider value={contextValue}>
        <Wrapper>
          <Filters />
          <JobList />
        </Wrapper>
      </FilterContext.Provider>
    </ApolloProvider>
  );
}

export default App;
const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  overflow: hidden;
`;



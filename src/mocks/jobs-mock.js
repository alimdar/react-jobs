import {QUERY_JOBS} from "../queries/Jobs";

export const mockThreeJobs = [
  {
    request: {
      query: QUERY_JOBS,
      variables: {
        search: null,
        city: null,
        company: null,
        investor: null,
        offset: 0,
        limit: 10
      }
    },
    result: {
      data: {
        jobs: [
          {
            id: 1,
            title: "Junior Dev",
            city: "Berlin",
            company: {
              name: "SomeNiceCompany",
              company_investors: [
                {investor: {name:"Rocket Internet"}},
                {investor: {name:"OneInvestor"}},
              ]
            }
          },
          {
            id: 2,
            title: "Senior Dev",
            city: "Berlin",
            company: {
              name: "SomeOtherCompany",
              company_investors: [
                {investor: {name:"OneInvestor"}},
              ]
            }
          },
          {
            id: 3,
            title: "Manager",
            city: "Berlin",
            company: {
              name: "SomeOtherCompany",
              company_investors: []
            }
          }
        ],
      },
    },
  },
];

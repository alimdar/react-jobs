import {createContext} from "react";
import {initialState} from "./reducer";

export const FilterContext = createContext(initialState);

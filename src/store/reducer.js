export const initialState = {
  search: "",
  city: null,
  company: null,
  investor: null
};

export const reducer = (state, action) => {
  if (action) {
    return {
      ...state,
      ...action,
    }
  }
  return state;
};

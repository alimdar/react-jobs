import {gql} from "apollo-boost";

const QUERY_FILTER_OPTIONS = gql`
    query fetchFilterOptions {
        companies {
            id
            name
        }
        jobs(distinct_on: city) {
            city
        }
        investors {
            id
            name
        }
    }
`;

export {
  QUERY_FILTER_OPTIONS
};

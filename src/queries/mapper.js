// map job object
export const jobsMapper = (job) => ({
  id: job.id,
  title: job.title,
  city: job.city,
  company: job.company.name,
  investors: job.company.company_investors &&
    job.company.company_investors.map((item) => item.investor.name)
});

// map filters data to dropdown options
export const optionsMapper = (options) => ({
  companies: options.companies && options.companies.map((company) => ({label: company.name, value: company.id})),
  cities: options.jobs && options.jobs.map((job) => ({label: job.city, value: job.city})),
  investors: options.investors && options.investors.map((inv) => ({label: inv.name, value: inv.id}))
});

import {gql} from "apollo-boost";

const QUERY_JOBS = gql`

    query fetchJobs(
        
        $city: String,
        $company: Int,
        $investor: Int,
        $search: String,
        $offset: Int,
        $limit:Int ) {

        jobs(
            offset: $offset, limit: $limit,
            where: {_or: {title: {_ilike: $search},
                city: {_eq: $city},
                company_id: {_eq: $company},
                company: {company_investors: {investor_id: {_eq: $investor}}}
            }}) {
            id
            title
            city
            company {
                name
                company_investors {
                    investor {
                        name
                    }
                }
            }
        }
    }
`;

export {
  QUERY_JOBS,
};

import React from "react";
import Select from 'react-select';
import {fontSize} from "../theme";
import {FilterName} from "./general-componants";

const customStyles = {
  option: (provided) => ({
    ...provided,
    fontSize: fontSize.m
  }),
  control: (provided) => ({
    ...provided,
    fontSize: fontSize.m
  })
};

const Dropdown = ({selectedValue, options, onSelect, title}) => {
  return (
    <div>
      {title && <FilterName>{title}</FilterName>}
      <Select
        styles={customStyles}
        isClearable={true}
        value={selectedValue}
        onChange={onSelect}
        options={options}
      />
    </div>
  )
};
export default Dropdown;

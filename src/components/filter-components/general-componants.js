import {colors, fontSize} from "../theme";
import styled from "styled-components";

export const FilterName = styled.div`
  font-size: ${fontSize.m};
  color: ${colors.fontLighter};
  color: ${colors.main};
  margin: 20px 0 8px 0;
`;

export const Button = styled.div`
  color: white;
  background-color: ${colors.main};
  font-size: ${fontSize.s};
  height: 30px;
  display: inline-block;
  padding: 6px 30px;
  text-align: center;
  border-radius: 4px;
  cursor: pointer;
  margin: 20px 0;
  &:hover {
    background-color: ${colors.lighter}
  }
`;

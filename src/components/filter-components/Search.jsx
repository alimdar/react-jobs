import React from "react";
import styled from "styled-components";
import {colors, fontSize} from "../theme";
import {FilterName} from "./general-componants";

const Search = ({value, onChange, resetValue, submitSearch}) => {
  return (
    <SearchWrapper>
      <FilterName>Title</FilterName>
      <InputComponent
        placeholder={"Search on enter"}
        value={value}
        onChange={onChange}
        onKeyUp={(e) => e.key === 'Enter' && submitSearch()}
      />
      <ClearFilter onClick={resetValue}>x</ClearFilter>
    </SearchWrapper>
  )
};
export default Search;

const SearchWrapper = styled.div`
  position: relative;
  height: 66px;
`;

const ClearFilter = styled.div`
  position: absolute;
  top: 33px;
  right: 10px;
  font-size: ${fontSize.l};
  font-weight: bold;
  color: ${colors.fontLighter};
  cursor: pointer;
`;

const InputComponent = styled.input`
  border: 1px solid;
  border-color: ${colors.border};
  outline: none;
  box-shadow: none;
  height: 38px;
  width: 100%;
  border-radius: 4px;
  padding: 0 30px 0 10px;
  color: ${colors.fontDefault};
  font-size: ${fontSize.m}  
`;

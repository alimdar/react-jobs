import React from 'react';
import {MockedProvider} from '@apollo/react-testing';
import {render} from '@testing-library/react';
import {act} from "react-dom/test-utils";
import Filters from './Filters';
import {QUERY_FILTER_OPTIONS} from "../../../queries/Filters";
import {FilterContext} from "../../../store/context";
import {initialState} from "../../../store/reducer";
import wait from 'waait';

test("should render all filters", async () => {
  const mocks = [
    {
      request: {
        query: QUERY_FILTER_OPTIONS,
      },
      result: {
        data: {
          jobs: [{city: 'Berlin'}, {city: "London"}],
          companies: [{name: 'BigCompany', id: 11}],
          investors: [{id: 1, name: 'BigInvestor'}, {id: 2, name: 'SmallInvestor'}],
        },
      },
    },
  ];
  await act(async () => {
      const {getByText} = render(
        <FilterContext.Provider value={{state: initialState}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <Filters/>
          </MockedProvider>
        </FilterContext.Provider>
      );
      await wait(0);
      expect(getByText('City')).toBeInTheDocument();
      expect(getByText('Title')).toBeInTheDocument();
      expect(getByText('Company')).toBeInTheDocument();
      expect(getByText('Investor')).toBeInTheDocument();
      expect(getByText('Reset all')).toBeInTheDocument();
    }
  );
});

test("should render error", async () => {
  const mocks = [
    {
      request: {
        query: QUERY_FILTER_OPTIONS,
      },
      result: {
        error: []
      },
    },
  ];
  await act(async () => {
      const {getByText} = render(
        <FilterContext.Provider value={{state: initialState}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <Filters/>
          </MockedProvider>
        </FilterContext.Provider>
      );
      await wait(0);
      expect(getByText('Error fetching filters')).toBeInTheDocument();
    }
  );
});

test("should render loading state", async () => {
  const mocks = [
    {
      request: {
        query: QUERY_FILTER_OPTIONS,
      },
      result: {
        error: []
      },
    },
  ];
  await act(async () => {
      const {getByText} = render(
        <FilterContext.Provider value={{state: initialState}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <Filters/>
          </MockedProvider>
        </FilterContext.Provider>
      );
      expect(getByText('Loading filters...')).toBeInTheDocument();
    }
  );
});

import React, {useContext, useState} from "react";
import styled from "styled-components";
import {useQuery} from "@apollo/react-hooks";
import {QUERY_FILTER_OPTIONS} from "../../../queries/Filters";
import {optionsMapper} from "../../../queries/mapper";
import Search from "../Search";
import {colors} from "../../theme";
import Dropdown from "../Dropdown";
import Info from "../../job-list-components/Info";
import {Button} from "../general-componants";
import {initialState} from "../../../store/reducer";
import {FilterContext} from "../../../store/context";

const Filters = () => {
  const {state, dispatch} = useContext(FilterContext);
  const {city, company, investor} = state;
  const [searchValue, setSearchValue] = useState("");

  const {loading, error, data} = useQuery(QUERY_FILTER_OPTIONS);
  const filterOptions = data && optionsMapper(data);

  if (loading) return <Info color={colors.lighter}>Loading filters...</Info>;
  if (error) return <Info color={colors.error}>Error fetching filters</Info>;

  return (
      <FilterWrapper>
        <Search value={searchValue}
                onChange={(e) => setSearchValue(e.target.value)}
                submitSearch={() => dispatch({search: searchValue})}
                resetValue={() => {
                  setSearchValue("");
                  dispatch({search: ""})
                }}
        />

        {filterOptions.companies && filterOptions.companies.length > 0 && <Dropdown
          title={"Company"}
          options={filterOptions.companies}
          selectedValue={company}
          onSelect={(v) => dispatch({company: v})}
        />}

        {filterOptions.cities && filterOptions.cities.length > 0 && <Dropdown
          title={"City"}
          options={filterOptions.cities}
          selectedValue={city}
          onSelect={(v) => dispatch({city: v})}
        />}

        {filterOptions.investors && filterOptions.investors.length > 0 && <Dropdown
          title={"Investor"}
          options={filterOptions.investors}
          selectedValue={investor}
          onSelect={(v) => dispatch({investor: v})}
        />}

        <Button
          onClick={() => {
            dispatch(initialState);
            setSearchValue("")
          }}>Reset all</Button>

      </FilterWrapper>
  )
};
export default Filters;

const FilterWrapper = styled.div`
  height: 100vh;
  width: 300px;
  padding: 30px 20px;
`;

export const colors = {
  border: "#dfdfdf",
  fontDefault: "#676767",
  fontLighter: "#858585",
  main: "#2684FF",
  lighter: "rgba(37,132,255,0.7)",
  error: "#f95b58"
};

export const fontSize = {
  xs: "8px",
  s: "12px",
  m: "14px",
  l: "16px",
  xl: "24px"
};

import React from "react";
import styled from "styled-components";
import {fontSize} from "../theme";

const Info = ({color, children}) => (
    <InfoWrapper style={{color: color}}>{children}</InfoWrapper>
);

export default Info;

const InfoWrapper = styled.div`
  margin: 50px 0;
  height: 40px;
  width: 300px;
  padding: 10px 30px;
  font-size: ${fontSize.m};
  font-weight: bold;
`;

import React from 'react';
import {MockedProvider} from '@apollo/react-testing';
import {render} from '@testing-library/react';
import {act} from "react-dom/test-utils";
import JobList from './JobList';
import {QUERY_JOBS} from "../../../queries/Jobs";
import {FilterContext} from "../../../store/context";
import {initialState} from "../../../store/reducer";
import wait from 'waait';
import {mockThreeJobs} from "../../../mocks/jobs-mock";

test("should render 3 jobs", async () => {
  await act(async () => {
      const {getByText} = render(
        <FilterContext.Provider value={{state: initialState}}>
          <MockedProvider mocks={mockThreeJobs} addTypename={false}>
            <JobList />
          </MockedProvider>
        </FilterContext.Provider>
      );
      await wait(0);
      expect(getByText('Junior Dev')).toBeInTheDocument();
      expect(getByText('Senior Dev')).toBeInTheDocument();
      expect(getByText('Manager')).toBeInTheDocument();
    }
  );
});

test("should render error", async () => {
  const mocks = [
    {
      request: {
        query: QUERY_JOBS,
      },
      result: {
        error: []
      },
    },
  ];
  await act(async () => {
      const {getByText} = render(
        <FilterContext.Provider value={{state: initialState}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <JobList />
          </MockedProvider>
        </FilterContext.Provider>
      );
      await wait(0);
      expect(getByText('Error')).toBeInTheDocument();
    }
  );
});

test("should render loading state", async () => {
  const mocks = [
    {
      request: {
        query: QUERY_JOBS,
      },
      result: {
        data: []
      },
    },
  ];
  await act(async () => {
      const {getByText} = render(
        <FilterContext.Provider value={{state: initialState}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <JobList />
          </MockedProvider>
        </FilterContext.Provider>
      );
      expect(getByText('Loading ...')).toBeInTheDocument();
    }
  );
});

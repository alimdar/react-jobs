import React, {useContext, useEffect, useState} from "react";
import {useQuery} from "@apollo/react-hooks";
import {QUERY_JOBS} from "../../../queries/Jobs";
import JobItem from "../job-item/JobItem";
import Info from "../Info";
import {colors} from "../../theme";
import {jobsMapper} from "../../../queries/mapper";
import styled from "styled-components";
import {FilterContext} from "../../../store/context";
import {Button} from "../../filter-components/general-componants";

const PAGE_SIZE = 10;

const JobList = () => {
  const {state} = useContext(FilterContext);
  const {city, company, investor, search} = state;
  const [fetchedAllData, setFlag] = useState(false);
  const [requestedMore, requestMore] = useState(false);

  const {loading, error, data, fetchMore} = useQuery(QUERY_JOBS, {
      variables: {
        city: city && city.value,
        company: company && company.value,
        investor: investor && investor.value,
        search: search ? `%${search}%` : null,
        offset: 0,
        limit: PAGE_SIZE
      }
    }
  );

  const fetchMoreResults = () => {
    fetchMore({
      variables: {offset: data.jobs.length},
      updateQuery: ((prev, {fetchMoreResult}) => {
        // set requested more flag to show/hide fetch more button
        requestMore(true);
        if (fetchMoreResult.jobs.length < PAGE_SIZE) {
          setFlag(true);
          return prev;
        }
        setFlag(false);
        return Object.assign({}, prev, {
          jobs: [...prev.jobs, ...fetchMoreResult.jobs],
        });
      })
    });
  };

  // reset "request more flag" on filter change
  useEffect(() => {
    requestMore(false);
  }, [state]);

  // show/hide "fetch more" button based on initial result number
  useEffect(() => {
    if (requestedMore) {
      return;
    }
    setFlag(data && data.jobs.length < PAGE_SIZE);
  }, [data, requestedMore]);

  if (loading) return <Info color={colors.lighter}>Loading ...</Info>;
  if (error) return <Info color={colors.error}>Error</Info>;

  return (
    <JobItemsWrapper>
      {data.jobs.length > 0 ?
        <div>
          {data.jobs.map((item) =>
            jobsMapper(item)).map(({id, title, city, company, investors}) =>
            <JobItem
              key={id}
              id={id}
              title={title}
              city={city}
              investors={investors}
              company={company}/>)}

          {!fetchedAllData && <Button onClick={() => fetchMoreResults()}>Fetch more</Button>}

        </div> :
        <Info color={colors.lighter}>No jobs found :( </Info>
      }
    </JobItemsWrapper>
  );
};
export default JobList;

export const JobItemsWrapper = styled.div`
  width: calc(100% - 300px);
  padding: 20px;
  overflow-y:auto;
  height: 100vh;
`;

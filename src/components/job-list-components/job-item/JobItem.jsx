import React from "react";
import styled from "styled-components";
import {colors, fontSize} from "../../theme";

const JobItem = ({title, city, company, investors}) => {

  return (
    <ItemWrapper>
      <ItemTitle>{title}</ItemTitle>
      <div>
        <Data><Label>Company:</Label><b>{company}</b></Data>
        <Data><Label>City:</Label><b>{city}</b></Data>
        {investors && investors.length > 0 &&
          <Data>
            <Label>Investors:</Label>
            <b>{investors.map((inv) => inv).join(', ')}</b>
          </Data>
        }
      </div>
    </ItemWrapper>
  )
};

export default JobItem;

const ItemWrapper = styled.div`
  min-height: 80px;
  border: 1px solid ${colors.border};
  border-radius: 5px;
  margin: 8px 0;
  padding: 10px 20px;
`;

const ItemTitle = styled.div`
  font-size: ${fontSize.m};
  color: ${colors.main};
  font-weight: bold;
`;

const Data = styled.div`
  font-size: ${fontSize.s};
  margin: 5px 0;
  color: ${colors.fontLighter}
`;

const Label = styled.div`
  width: 60px;
  display: inline-block;
`;

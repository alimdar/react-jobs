import React from 'react';
import { render } from '@testing-library/react';
import JobItem from './JobItem';

test('renders JobItem component', () => {
  const investors = ["One", "Two"];
  const { getByText } = render(<JobItem
    city={"Berlin"}
    title={"Developer"}
    company={"BigCompany"}
    investors={investors}/>);
  const cityName = getByText(/Berlin/);
  const companyName = getByText(/BigCompany/);
  const titleName = getByText(/Developer/);
  const investorsName = getByText(/One, Two/);
  expect(cityName).toBeInTheDocument();
  expect(companyName).toBeInTheDocument();
  expect(titleName).toBeInTheDocument();
  expect(investorsName).toBeInTheDocument();
});


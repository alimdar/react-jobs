import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders content in App', () => {
  const { container } = render(<App />);
  expect(container.childElementCount).toBe(1);
});
